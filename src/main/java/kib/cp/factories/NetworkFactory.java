package kib.cp.factories;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class NetworkFactory {
	
	
	private NetworkFactory() {}
	
	public static String[] getOrigins() {
		List<String> ips = new ArrayList<>();
		ips.add("localhost");
		Enumeration<NetworkInterface> e;
		try {
			e = NetworkInterface.getNetworkInterfaces();
			while(e.hasMoreElements()){
			    NetworkInterface n = (NetworkInterface) e.nextElement();
			    Enumeration<InetAddress> ee = n.getInetAddresses();
			    while (ee.hasMoreElements()){
			        InetAddress i = (InetAddress) ee.nextElement();
			        //System.out.println("Host:"+i.getHostAddress());
			        ips.add(i.getHostAddress());
			    }
			}
		} catch (SocketException e1) {
			e1.printStackTrace();
		}
		return ips.stream().map(ip->ip+":8081").peek(host->System.out.println("Host: "+host)).toArray(size->new String[size]);
	}

}
