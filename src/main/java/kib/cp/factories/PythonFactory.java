package kib.cp.factories;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.python.core.PyObject;
import org.python.util.PythonInterpreter;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import kib.cp.dtos.ParamDTO;
import kib.cp.dtos.ParametersDTO;
import kib.cp.dtos.SourceDataDTO;
import kib.cp.py.CalculationResult;
import kib.cp.py.ClassCountingsEvaluationResultDTO;
/**
 * @license
 * Copyright (c) 2018 Bernhard Kinast. All rights reserved.
 * Except where otherwise noted, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 */
public class PythonFactory {

	private PythonFactory() {

	}

	public static void main(String[] args) throws IOException {
		//String code = getScriptCode("klassengrenzen");
		//System.out.println(code);
		/*
		Map<String, String> params = getParametersForScript("first_script");
		for (Entry<String, String> param : params.entrySet()) {
			System.out.println(
					"Param:" + param.getKey() + (param.getValue() == null ? "" : (" value:" + param.getValue())));
		}
		executePythonScript("first_script.py", null, null);
		*/
	}

	// public static List<String> getDataHeadersFroScript(String script){
	// script = script.replaceAll(".py", "");
	// Properties p = new Properties();
	// p=setDefaultPythonPath(p);
	// PythonInterpreter.initialize(System.getProperties(), p, new String[] {});
	// PythonInterpreter py = new PythonInterpreter();
	// py.exec("import inspect");
	// py.exec("import " + script);
	// py.exec("from " + script + " import calculate");
	// py.exec("params,_,_,values = inspect.getargspec(calculate)");
	// PyObject pyObject = py.get("params");
	// Map<String,String> parameterMap = new HashMap<>();
	// List<String> params = new ArrayList<String>();
	// pyObject.asIterable().forEach(param->params.add(param.asString()));
	// PyObject defaultValues = py.get("values");
	// List<String> defaults = new ArrayList<>();
	// defaultValues.asIterable().forEach(value -> defaults.add(value.toString()));
	// for(int i = 0;i<params.size();i++) {
	// if(defaults.size()>=params.size()-i) {
	// parameterMap.put(params.get(i),
	// defaults.get(i-params.size()+defaults.size()));
	// }else {
	// parameterMap.put(params.get(i), null);
	// }
	// }
	// return parameterMap;
	// }
	
	public static Resource[] listAllPythonScripts(String directory) throws IOException {
		//ClassLoader cl = PythonFactory.class.getClassLoader();
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		ResourcePatternResolver resourceResolver = new PathMatchingResourcePatternResolver(cl);
		Resource[] resources;
		resources = resourceResolver.getResources("classpath*:/static/py/"+directory+"/*.py");
		resources = Arrays.stream(resources).filter(resource->!resource.getFilename().equals("__init__.py")).toArray(size->new Resource[size]);
		return resources;
	}
	
	public static String getScriptCode(String directory,String scriptName) throws IOException {
		scriptName = scriptName.replaceAll(".py", "");
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		ResourcePatternResolver resourceResolver = new PathMatchingResourcePatternResolver(cl);
		Resource[] resources;
		resources = resourceResolver.getResources("classpath*:/static/py/"+directory+"/"+scriptName+".py");
		if(resources.length<1) {
			return "";
		}
		Resource res = resources[0];
		try(InputStream in = res.getInputStream();
			InputStreamReader r = new InputStreamReader(in);
			BufferedReader bf = new BufferedReader(r)){
			StringBuffer content = new StringBuffer();
			String line;
			while((line = bf.readLine())!=null) {
				content.append(line).append("\n");
			}
			return content.toString();
		}
	}

	public static Map<String, String> getParametersForScript(String directory, String script) {
		script = script.replaceAll(".py", "");
		Properties p = new Properties();
		p = setDefaultPythonPath(p);
		PythonInterpreter.initialize(System.getProperties(), p, new String[] {});
		
		PythonInterpreter py = new PythonInterpreter();
		py.exec("import sys");
		// clear all modules because it doesn't cost much time to reload the modules and if we reload them we can reflect changes to the script immediately
		py.exec("sys.modules.clear()");
		py.exec("print(\"py-path %s\" % sys.path)");
		py.exec("import inspect");
		py.exec("from kib.cp.py import *");
		py.exec("import "+directory+"." + script);
		py.exec("from "+directory+"." + script + " import calculate");
		py.exec("params,varargs,keywords,values = inspect.getargspec(calculate)");
		py.exec("print(\"params: %s\" % params)");
		py.exec("varargs = \"null\" if varargs is None else [x if x is not None else \"null\" for x in varargs]");
		py.exec("print(\"varargs: %s\" % varargs)");
		py.exec("keywords = \"null\" if keywords is None else [x if x is not None else \"null\" for x in keywords]");
		py.exec("print(\"keywords: %s\" % keywords)");
		py.exec("values = \"null\" if values is None else [x if x is not None else \"null\" for x in values]");
		py.exec("print(\"values: %s\" % values)");
		PyObject pyObject = py.get("params");
		Map<String, String> parameterMap = new LinkedHashMap<>();
		List<String> params = new ArrayList<String>();
		pyObject.asIterable().forEach(param -> {
			params.add(param.asString());
		});
		PyObject defaultValues = py.get("values");
		List<String> defaults = new ArrayList<>();
		defaultValues.asIterable().forEach(value -> {
			if (value.isNumberType()) {
				defaults.add(value.toString());
			} else {
				defaults.add("\"" + value.toString() + "\"");
			}
		});
		for (int i = 0; i < params.size(); i++) {
			if (defaults.size() >= params.size() - i) {
				parameterMap.put(params.get(i), defaults.get(i - params.size() + defaults.size()));
			} else {
				parameterMap.put(params.get(i), null);
			}
		}
		//py.close();
		return parameterMap;
	}

	public static CalculationResult executePythonScript(String directory, String script, ParametersDTO parametersDTO, SourceDataDTO data) {

		Map<String, String> realParams = getParametersForScript(directory,script);

		script = script.replaceAll(".py", "");
		System.out.println("Running Script: " + script);
		Properties p = new Properties();
		p = setDefaultPythonPath(p);
		System.out.println("Python path:" + p.getProperty("python.path"));
		PythonInterpreter.initialize(System.getProperties(), p, new String[] {});
		PythonInterpreter py = new PythonInterpreter();
		py.exec("import sys");
		// clear all modules because it doesn't cost much time to reload the modules and if we reload them we can reflect changes to the script immediately
		py.exec("sys.modules.clear()");
		py.exec("from kib.cp.py import *");
		py.exec("import "+directory+"." + script);
		System.out.println("imported script");
		py.set("data", data.getDataAsString());
		py.exec("datalines = data.splitlines()");
		try {
			py.exec("data = [[float(value) for value in line.split(\";\")] for line in datalines]");
		}catch(Exception ex) {
			System.out.println("Unable to cast from String to float, trying to take value as it is!");
			py.exec("data = [[value for value in line.split(\";\")] for line in datalines]");
		}
		System.out.println("set data for script:");

		StringBuilder builder = new StringBuilder();
		for (ParamDTO param : parametersDTO.getParams()) {
			System.out.println("Setting Parameter: " + param.getName() + " value: " + param.getValue());
			py.exec(param.getName() + "=" + param.getValue());
		}
		boolean first = true;
		for (Entry<String, String> param : realParams.entrySet()) {
			if (first) {
				first = false;
				builder.append(param.getKey());
			} else {
				builder.append(",").append(param.getKey());
			}
		}

		py.exec("from "+directory+"." + script + " import calculate");
		System.out.println("Going to execute the script...");
		py.exec("result = calculate(" + builder.toString() + ")");
		System.out.println("Script finished!");
		py.exec("print(result)");
		py.exec("from helpers.java_converter import convertToJava");
		py.exec("result = convertToJava(result)");
		PyObject result = py.get("result");
		CalculationResult javaResult = (CalculationResult) result.__tojava__(CalculationResult.class);
		System.out.println("Result = " + javaResult);
		//py.close();
		return javaResult;
	}

	public static ClassCountingsEvaluationResultDTO executeClassCountingsEvaluationScript(String directory, String script, ParametersDTO parametersDTO, Object[] data) {
		Map<String, String> realParams = getParametersForScript(directory,script);

		script = script.replaceAll(".py", "");
		System.out.println("Running Script: " + script);
		Properties p = new Properties();
		p = setDefaultPythonPath(p);
		System.out.println("Python path:" + p.getProperty("python.path"));
		PythonInterpreter.initialize(System.getProperties(), p, new String[] {});
		PythonInterpreter py = new PythonInterpreter();
		py.exec("import sys");
		// clear all modules because it doesn't cost much time to reload the modules and if we reload them we can reflect changes to the script immediately
		py.exec("sys.modules.clear()");
		py.exec("from kib.cp.py import *");
		py.exec("import "+directory+"." + script);
		System.out.println("imported script");
		py.set("data", data);
		//py.exec("datalines = data.splitlines()");
		//try {
		//	py.exec("data = [[float(value) for value in line.split(\";\")] for line in datalines]");
		//}catch(Exception ex) {
		//	System.out.println("Unable to cast from String to float, trying to take value as it is!");
		//	py.exec("data = [[value for value in line.split(\";\")] for line in datalines]");
		//}
		System.out.println("set data for script:");

		StringBuilder builder = new StringBuilder();
		for (ParamDTO param : parametersDTO.getParams()) {
			System.out.println("Setting Parameter: " + param.getName() + " value: " + param.getValue());
			py.exec(param.getName() + "=" + param.getValue());
		}
		boolean first = true;
		for (Entry<String, String> param : realParams.entrySet()) {
			if (first) {
				first = false;
				builder.append(param.getKey());
			} else {
				builder.append(",").append(param.getKey());
			}
		}

		py.exec("from "+directory+"." + script + " import calculate");
		System.out.println("Going to execute the script...");
		py.exec("result = calculate(" + builder.toString() + ")");
		System.out.println("Script finished!");
		py.exec("print(result)");
		py.exec("from helpers.java_converter_class_countings import convertToJava");
		py.exec("result = convertToJava(result)");
		PyObject result = py.get("result");
		ClassCountingsEvaluationResultDTO javaResult = (ClassCountingsEvaluationResultDTO) result.__tojava__(ClassCountingsEvaluationResultDTO.class);
		System.out.println("Result = " + javaResult);
		//py.close();
		return javaResult;
	}


	/**
	 * Adds user.dir into python.path to make Jython look for python modules in
	 * working directory in all cases (both standalone and not standalone modes)
	 * 
	 * @param props
	 * @return props
	 */
	/*private static Properties setDefaultPythonPath(Properties props) {
		// System.out.println(PythonFactory.class.getClassLoader().getResource("static/py").toExternalForm());
		// p.setProperty("python.path", System.getProperty("user.dir"));
		// p.setProperty("python.home", System.getProperty("user.dir"));
		// p.setProperty("python.prefix", System.getProperty("user.dir"));
		String pythonPathProp = props.getProperty("python.path");
		String subpath = "/src/main/resources/static/py/";
		String javaClassPath = "/src/main/java/";
		String javaClassPath2 = System.getProperty("java.class.path").replaceAll("-spring-boot", "");
		String springLibPath = System.getProperty("java.class.path")+"/BOOT-INF/lib/Lib";
		System.out.println("java classpath is: "+javaClassPath2);
		//String subpath = "/";
		String new_value;
		if (pythonPathProp == null) {
			new_value = System.getProperty("user.dir") + subpath;
		} else {
			new_value = pythonPathProp + java.io.File.pathSeparator + System.getProperty("user.dir") + subpath
					+ java.io.File.pathSeparator;
		}
		System.out.println("executed from: "+javaClassPath2);
		new_value = new_value + java.io.File.pathSeparator + System.getProperty("user.dir")+javaClassPath + java.io.File.pathSeparator;
		if(!javaClassPath2.endsWith(".jar")) {
		}else {
			//new_value = new_value + java.io.File.pathSeparator + System.getProperty("user.dir")+ "/"+javaClassPath2;
			//new_value = new_value + java.io.File.pathSeparator+System.getProperty("user.dir")+ "/"+springLibPath;
		}
		props.setProperty("python.path", new_value);
		String formatedPath = new_value.replaceAll(";", ";\n");
		System.out.println("Python path set to:\n"+formatedPath);
		props.setProperty("python.console.encoding", "UTF-8");
		//System.getProperties().forEach((k,v)->{
		//	System.out.println(k+"="+v);
		//});
		
		return props;
	}*/
	private static Properties setDefaultPythonPath(Properties props) {
		// System.out.println(PythonFactory.class.getClassLoader().getResource("static/py").toExternalForm());
		// p.setProperty("python.path", System.getProperty("user.dir"));
		// p.setProperty("python.home", System.getProperty("user.dir"));
		// p.setProperty("python.prefix", System.getProperty("user.dir"));
		String pythonPathProp = props.getProperty("python.path");
		String subpath = "/src/main/resources/static/py/";
		String javaClassPath = "/src/main/java/";
		String classPath =  System.getProperty("java.class.path");
		boolean startedFromJar = System.getProperty("sun.java.command").contains(".jar");
		
		//String subpath = "/";
		String new_value;
		new_value = classPath + java.io.File.pathSeparator;
		if (pythonPathProp == null) {
			new_value = System.getProperty("user.dir") + subpath;
		} else {
			new_value = pythonPathProp + java.io.File.pathSeparator + System.getProperty("user.dir") + subpath
					+ java.io.File.pathSeparator;
		}
		//if(startedFromJar) {
		//	new_value = new_value + java.io.File.pathSeparator + System.getProperty("user.dir")+"/"+classPath+"!/lib/" + java.io.File.pathSeparator;
		//}else {			
			new_value = new_value + java.io.File.pathSeparator + System.getProperty("user.dir")+javaClassPath + java.io.File.pathSeparator;
		//}
		
		
		System.getProperties().forEach((k,v)->{
			if((""+k).contains("python")) {
				System.out.println(k+"="+v);
			}
		});
		props.setProperty("python.console.encoding", "UTF-8");
		//props.setProperty("python.packages.directories", System.getProperty("user.dir")+javaClassPath + java.io.File.pathSeparator);
		props.setProperty("python.path", new_value);
		System.out.println("---------------------------------------------------");
		props.forEach((k,v)->{
			System.out.println(k+"="+v);
		});
		System.out.println("---------------------------------------------------");
		return props;
	}

}
