package kib.cp.factories;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
/**
 * @license
 * Copyright (c) 2018 Bernhard Kinast. All rights reserved.
 * Except where otherwise noted, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 */
public class ExampleFactory {
	
	private ExampleFactory() {
		
	}

	public static Resource[] listAllExampleData() throws IOException {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		ResourcePatternResolver resourceResolver = new PathMatchingResourcePatternResolver(cl);
		Resource[] resources;
		resources = resourceResolver.getResources("classpath*:/static/csv/exampledata/*.csv");
		return resources;
	}
	
	public static String getExampleData(String exampleName) throws IOException {
		exampleName = exampleName.replaceAll(".csv", "");
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		ResourcePatternResolver resourceResolver = new PathMatchingResourcePatternResolver(cl);
		Resource[] resources;
		resources = resourceResolver.getResources("classpath*:/static/csv/exampledata/"+exampleName+".csv");
		if(resources.length<1) {
			return "";
		}
		Resource res = resources[0];
		try(InputStream in = res.getInputStream();
			InputStreamReader r = new InputStreamReader(in);
			BufferedReader bf = new BufferedReader(r)){
			StringBuffer content = new StringBuffer();
			String line;
			while((line = bf.readLine())!=null) {
				content.append(line).append("\n");
			}
			return content.toString();
		}
	}
}
