package kib.cp.py;

public class CalculationClass {

	private double min=0;
	
	private double max=0;
	
	public CalculationClass() {
	}
	
	public CalculationClass(double min, double max) {
		this.min=min;
		this.max=max;
	}

	public double getMin() {
		return min;
	}

	public void setMin(double min) {
		this.min = min;
	}

	public double getMax() {
		return max;
	}

	public void setMax(double max) {
		this.max = max;
	}

	public double getRange() {
		return max-min;
	}
	
	
}
