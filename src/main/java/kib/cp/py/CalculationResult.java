package kib.cp.py;

import java.util.ArrayList;
import java.util.List;

public class CalculationResult {
	
	private List<CalculationClass> calculationClasses = new ArrayList<>();
	
	private List<Integer> classCrossings = new ArrayList<>();
	
	private List<Integer> rangeCounts = new ArrayList<>();
	
	private List<Integer> usedIndizes = new ArrayList<>();
	
	private Double[][] pointsOfInterest = new Double[0][0];
	
	private Double[][] markovProbabilities = new Double[0][0];
	
	private Integer[][] gridFromTo = new Integer[0][0];

	public List<CalculationClass> getCalculationClasses() {
		return calculationClasses;
	}

	public void setCalculationClasses(List<CalculationClass> calculationClasses) {
		this.setCalculationClasses(calculationClasses);
	}

	public List<Integer> getClassCrossings() {
		return classCrossings;
	}

	public void setClassCrossings(List<Integer> classCrossings) {
		this.classCrossings = classCrossings;
	}

	public List<Integer> getRangeCounts() {
		return rangeCounts;
	}

	public void setRangeCounts(List<Integer> rangeCounts) {
		this.rangeCounts = rangeCounts;
	}

	public Double[][] getMarkovProbabilities() {
		return markovProbabilities;
	}

	public void setMarkovProbabilities(Double[][] markovProbabilities) {
		this.markovProbabilities = markovProbabilities;
	}

	public List<Integer> getUsedIndizes() {
		return usedIndizes;
	}

	public void setUsedIndizes(List<Integer> usedIndizes) {
		this.usedIndizes = usedIndizes;
	}

	public Double[][] getPointsOfInterest() {
		return pointsOfInterest;
	}

	public void setPointsOfInterest(Double[][] pointsOfInterest) {
		this.pointsOfInterest = pointsOfInterest;
	}

	public Integer[][] getGridFromTo() {
		return gridFromTo;
	}

	public void setGridFromTo(Integer[][] gridFromTo) {
		this.gridFromTo = gridFromTo;
	}
	
	

}
