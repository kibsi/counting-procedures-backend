package kib.cp.py;

public class ClassCountingsEvaluationResultDTO {
	int[] levels;
	double[] tensions;
	double[] sumAmounts;
	double[] levelAmounts;
	double[] maxTensions;
	double[] tolerableNumberOfCycles;
	double[] damageParts;
	double totalDamage;
	double[] percentDamage;
	//double[][] pointsOfInterest;
	
	public int[] getLevels() {
		return levels;
	}
	public void setLevels(int[] levels) {
		this.levels = levels;
	}
	public double[] getTensions() {
		return tensions;
	}
	public void setTensions(double[] tensions) {
		this.tensions = tensions;
	}
	public double[] getSumAmounts() {
		return sumAmounts;
	}
	public void setSumAmounts(double[] sumAmounts) {
		this.sumAmounts = sumAmounts;
	}
	public double[] getLevelAmounts() {
		return levelAmounts;
	}
	public void setLevelAmounts(double[] levelAmounts) {
		this.levelAmounts = levelAmounts;
	}
	public double[] getMaxTensions() {
		return maxTensions;
	}
	public void setMaxTensions(double[] maxTensions) {
		this.maxTensions = maxTensions;
	}
	public double[] getTolerableNumberOfCycles() {
		return tolerableNumberOfCycles;
	}
	public void setTolerableNumberOfCycles(double[] tolerableNumberOfCycles) {
		this.tolerableNumberOfCycles = tolerableNumberOfCycles;
	}
	public double[] getDamageParts() {
		return damageParts;
	}
	public void setDamageParts(double[] damageParts) {
		this.damageParts = damageParts;
	}
	public double getTotalDamage() {
		return totalDamage;
	}
	public void setTotalDamage(double totalDamage) {
		this.totalDamage = totalDamage;
	}
	public double[] getPercentDamage() {
		return percentDamage;
	}
	public void setPercentDamage(double[] percentDamage) {
		this.percentDamage = percentDamage;
	}
	/*
	public double[][] getPointsOfInterest() {
		return pointsOfInterest;
	}
	public void setPointsOfInterest(double[][] pointsOfInterest) {
		this.pointsOfInterest = pointsOfInterest;
	}
	*/
	
}
