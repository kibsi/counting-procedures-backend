package kib.cp.backend;

import java.util.Arrays;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.io.Resource;

import kib.cp.factories.PythonFactory;

/**
 * @license
 * Copyright (c) 2018 Bernhard Kinast. All rights reserved.
 * Except where otherwise noted, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 */
@ComponentScan(basePackages= {"kib.cp.restcontroller","kib.cp.dtos","kib.cp.backend"})
@SpringBootApplication
@EnableCaching
public class RestStarter {

	public static void main(String[] args) {
		System.setProperty( "server.port", "8090" );
		SpringApplication.run(RestStarter.class, args);
	}
	
	@Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {

            System.out.println("Let's inspect the beans provided by Spring Boot:");

            String[] beanNames = ctx.getBeanDefinitionNames();
            Arrays.sort(beanNames);
            for (String beanName : beanNames) {
                System.out.println(beanName);
            }
            System.out.println("-----------All beans inspected!");
            Resource[] scripts = PythonFactory.listAllPythonScripts("countingprocedures");
            for(Resource res:scripts) {
            	System.out.println("Available script: "+res.getFilename());
            }
        };
    }
	
	
}
