package kib.cp.dtos;

public class RunDataDTO {
	
	private String data = "";
	
	private ParametersDTO parameters;

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public ParametersDTO getParameters() {
		return parameters;
	}

	public void setParameters(ParametersDTO parameters) {
		this.parameters = parameters;
	}



}
