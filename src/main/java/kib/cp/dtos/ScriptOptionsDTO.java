package kib.cp.dtos;

import java.util.ArrayList;
import java.util.List;

public class ScriptOptionsDTO {
	
	private final List<ScriptOptionDTO> scripts = new ArrayList<>();
	

	public List<ScriptOptionDTO> getScripts() {
		return scripts;
	}

}
