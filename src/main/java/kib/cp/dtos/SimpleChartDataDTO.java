package kib.cp.dtos;

import java.io.Serializable;
import java.util.Random;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonView;
@Component
@Scope(value="session", proxyMode=ScopedProxyMode.TARGET_CLASS)
public class SimpleChartDataDTO implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public interface IViewData{};
	
	private String[] header;
	
	private double[][] data;
	
	public SimpleChartDataDTO() {
		header = new String[] {"time","generated"};
		Random rand = new Random();
		int length = 1000+rand.nextInt(1000);
		int med = rand.nextInt(1000);
		int sigma = rand.nextInt(100);
		data = new double[length][2];
		for(int i = 0;i<length;i++) {
			data[i][0] = i;
			data[i][1] = (int)Math.round(med+ sigma*rand.nextGaussian());
		}
	}
	
	public SimpleChartDataDTO(String[] header, double[][] data) {
		this.header=header;
		this.data=data;
	}
	
	@JsonView(IViewData.class)
	public String[] getHeader() {
		return header;
	}
	@JsonView(IViewData.class)
	public double[][] getData() {
		return data;
	}

	public void setHeader(String[] header) {
		this.header = header;
	}

	public void setData(double[][] data) {
		this.data = data;
	}
	
	

}
