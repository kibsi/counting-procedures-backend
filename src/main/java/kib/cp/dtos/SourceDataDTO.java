package kib.cp.dtos;

import java.io.Serializable;

//@Component
//@SessionScope(proxyMode=ScopedProxyMode.TARGET_CLASS)
//@Scope(value="session", proxyMode=ScopedProxyMode.TARGET_CLASS)
//@Component
//@SessionScope(proxyMode=ScopedProxyMode.TARGET_CLASS)
public class SourceDataDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String[][] data;
	
	private String dataAsString;
	
	public SourceDataDTO() {
		
	}
	
	public void setData(String[][] data) {
		this.data = data;
	}
	
	public String[][] getData(){
		return data;
	}

	public String getDataAsString() {
		return dataAsString;
	}

	public void setDataAsString(String dataAsString) {
		this.dataAsString = dataAsString;
	}
	
	

}
