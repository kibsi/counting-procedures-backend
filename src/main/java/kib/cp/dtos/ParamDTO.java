package kib.cp.dtos;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ParamDTO {
	
	
	private String name;
	
	private String value;

	@JsonCreator
	public ParamDTO(@JsonProperty("name") String name,@JsonProperty("value") String value) {
		super();
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	
	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	
	public void setValue(String value) {
		this.value = value;
	}
	
	

	
}
