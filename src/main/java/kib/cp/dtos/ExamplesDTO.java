package kib.cp.dtos;

import java.util.ArrayList;
import java.util.List;

public class ExamplesDTO {
	
	private List<ExampleDTO> examples = new ArrayList<>();

	public List<ExampleDTO> getExamples() {
		return examples;
	}

}
