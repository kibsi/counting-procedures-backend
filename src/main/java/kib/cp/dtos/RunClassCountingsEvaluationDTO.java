package kib.cp.dtos;

import java.util.ArrayList;
import java.util.List;

public class RunClassCountingsEvaluationDTO {
	
	private List<CalculationClassDTO> calculationClasses = new ArrayList<>();
	
	private ParametersDTO parameters;

	public ParametersDTO getParameters() {
		return parameters;
	}

	public void setParameters(ParametersDTO parameters) {
		this.parameters = parameters;
	}

	public List<CalculationClassDTO> getCalculationClasses() {
		return calculationClasses;
	}

	public void setCalculationClasses(List<CalculationClassDTO> calculationClasses) {
		this.calculationClasses = calculationClasses;
	}
	
}
