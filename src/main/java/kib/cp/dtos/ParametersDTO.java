package kib.cp.dtos;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ParametersDTO {
	
	//private final ObservableList<ParamDTO> params = FXCollections.observableArrayList();
	private List<ParamDTO> params = new ArrayList<>();
	//private final ObservableList<String> headers = FXCollections.observableArrayList();
	private List<String> headers = new ArrayList<>();
	
	
	public ParametersDTO() {
		
	}
	
	@JsonCreator
	public ParametersDTO(@JsonProperty("params") List<ParamDTO> params) {
		//this.params.addAll(params);
		this.params=params;
	}
	
	
	public List<ParamDTO> getParams(){
		return params;
	}
	
	public void setParams(List<ParamDTO> params) {
		this.params.addAll(params);
	}
	
	public List<String> getHeaders(){
		return headers;
	}
	
	public void setHeaders(List<String>headers) {
		//this.headers.addAll(headers);
		this.headers=headers;
	}

}
