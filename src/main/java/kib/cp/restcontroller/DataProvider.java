package kib.cp.restcontroller;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import kib.cp.dtos.SourceDataDTO;

@Component
public class DataProvider {

	@Bean
	@SessionScope(proxyMode=ScopedProxyMode.TARGET_CLASS)
	public SourceDataDTO createSourceData() {
		return new SourceDataDTO();
	}
}
