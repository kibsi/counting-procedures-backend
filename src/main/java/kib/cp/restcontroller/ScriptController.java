package kib.cp.restcontroller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;

import kib.cp.dtos.CalculationClassDTO;
import kib.cp.dtos.ParamDTO;
import kib.cp.dtos.ParametersDTO;
import kib.cp.dtos.RunClassCountingsEvaluationDTO;
import kib.cp.dtos.RunDataDTO;
import kib.cp.dtos.ScriptOptionDTO;
import kib.cp.dtos.ScriptOptionsDTO;
import kib.cp.dtos.SourceDataDTO;
import kib.cp.factories.PythonFactory;
import kib.cp.py.CalculationResult;
import kib.cp.py.ClassCountingsEvaluationResultDTO;

@RestController
@SessionAttributes("sourcedata")
/**
 * @license
 * Copyright (c) 2018 Bernhard Kinast. All rights reserved.
 * Except where otherwise noted, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 */
public class ScriptController {

	
	//private final SourceDataDTO sourceData;
	
	//@Autowired
	//private ScriptController(SourceDataDTO sourceData) {
	//	this.sourceData = sourceData;
	//}
	
	private static Pattern allowedParameterPattern = Pattern.compile("[^A-Za-z0-9\\.,;\\-\\_ \n\r]");

	//@CrossOrigin(origins = "http://127.0.0.1:8081")
	@CrossOrigin
	@Cacheable("scripts")
	@RequestMapping("/scripts")
	public ScriptOptionsDTO getAvailableScripts() throws IOException {
		Resource[] pyScripts = PythonFactory.listAllPythonScripts("countingprocedures");
		ScriptOptionsDTO options = new ScriptOptionsDTO();
		for (Resource resource : pyScripts) {
			ScriptOptionDTO dto = new ScriptOptionDTO();
			dto.setName(resource.getFilename().replaceAll(".py", ""));
			dto.setFile(resource.getFilename());
			dto.setCode(PythonFactory.getScriptCode("countingprocedures",dto.getName()));
			dto.setParameters(getParametersForScript("countingprocedures",dto.getName()));
			options.getScripts().add(dto);
		}
		return options;
	}
	
	@CrossOrigin
	@Cacheable("postcalculations")
	@RequestMapping("/postcalculations")
	public ScriptOptionsDTO getAvailablePostCalculations() throws IOException {
		Resource[] pyScripts = PythonFactory.listAllPythonScripts("postcalculations");
		ScriptOptionsDTO options = new ScriptOptionsDTO();
		for (Resource resource : pyScripts) {
			ScriptOptionDTO dto = new ScriptOptionDTO();
			dto.setName(resource.getFilename().replaceAll(".py", ""));
			dto.setFile(resource.getFilename());
			dto.setCode(PythonFactory.getScriptCode("postcalculations",dto.getName()));
			dto.setParameters(getParametersForScript("postcalculations",dto.getName()));
			options.getScripts().add(dto);
		}
		return options;
	}
	
	@CrossOrigin
	@RequestMapping("/classcountingsevaluations")
	public ScriptOptionsDTO getAvailableClassCountingEvaluations() throws IOException {
		Resource[] pyScripts = PythonFactory.listAllPythonScripts("classcountingsevaluations");
		ScriptOptionsDTO options = new ScriptOptionsDTO();
		for (Resource resource : pyScripts) {
			ScriptOptionDTO dto = new ScriptOptionDTO();
			dto.setName(resource.getFilename().replaceAll(".py", ""));
			dto.setFile(resource.getFilename());
			dto.setCode(PythonFactory.getScriptCode("classcountingsevaluations",dto.getName()));
			dto.setParameters(getParametersForScript("classcountingsevaluations",dto.getName()));
			options.getScripts().add(dto);
		}
		return options;
	}

	//@CrossOrigin(origins = "http://127.0.0.1:8081")
	@CrossOrigin
	@Cacheable("scriptnames")
	@RequestMapping(produces = "application/json", value = "/scriptnames")
	public List<String> getAvailableScriptNames() throws IOException {
		Resource[] pyScripts = PythonFactory.listAllPythonScripts("countingprocedures");
		List<String> names = new ArrayList<>();
		for (int i = 0; i < pyScripts.length; i++) {
			names.add(pyScripts[i].getFilename());
		}
		return names;
	}
	
	@CrossOrigin
	@Cacheable("postcalculationnames")
	@RequestMapping(produces = "application/json", value = "/postcalculationnames")
	public List<String> getAvailablePostCalculationNames() throws IOException {
		Resource[] pyScripts = PythonFactory.listAllPythonScripts("postcalculations");
		List<String> names = new ArrayList<>();
		for (int i = 0; i < pyScripts.length; i++) {
			names.add(pyScripts[i].getFilename());
		}
		return names;
	}
	
	//@CrossOrigin(origins = "http://127.0.0.1:8081")
	@CrossOrigin
	//@RequestMapping(produces = "application/json", value="/parameter")
	public ParametersDTO getParametersForScript(/*@RequestParam(name = "script")*/String directory, String scriptName) throws IOException {
		ParametersDTO dto = new ParametersDTO();
		Map<String, String> parameters = PythonFactory.getParametersForScript(directory,scriptName);
		for (Entry<String, String> entry : parameters.entrySet()) {
			if (entry.getValue().contains("[") && entry.getValue().contains("]")) {
				dto.setHeaders(Arrays.asList(entry.getValue().replaceAll("\\[", "").replaceAll("\\]", "")
						.replaceAll(" ", "").replaceAll("'", "").split(",")));
			} else {
				dto.getParams().add(new ParamDTO(entry.getKey(), entry.getValue()));
			}
		}
		return dto;
	}
	
	@CrossOrigin
	@RequestMapping(value = "/run/classcrossingsevaluation", method = RequestMethod.POST, consumes = "application/json")
	public ClassCountingsEvaluationResultDTO runClassCrossingsEvaluation(@RequestParam(name = "script") String scriptName, @RequestBody RunClassCountingsEvaluationDTO body)
			throws Exception {
		System.out.println("Unmodified headers:\n" + body.getParameters().getHeaders());
		
		for (ParamDTO param : body.getParameters().getParams()) {
			System.out.println("Name=" + param.getName() + " Value=" + param.getValue());
		}
		//SourceDataDTO sourceData = new SourceDataDTO();
		Object[][] data = new Object[body.getCalculationClasses().size()][4];
		int i=0;
		for (CalculationClassDTO calculationClass:body.getCalculationClasses()) {
			data[i][0] = calculationClass.getName();
			data[i][1] = calculationClass.getMin();
			data[i][2] = calculationClass.getMax();
			data[i][3] = calculationClass.getAmount();
			i++;
		}
		//sourceData.setData(data);
		return PythonFactory.executeClassCountingsEvaluationScript("classcountingsevaluations",scriptName, body.getParameters(), data);
	}
	
	//@CrossOrigin(origins = "http://127.0.0.1:8081")
	@CrossOrigin
	@RequestMapping(value = "/run", method = RequestMethod.POST, consumes = "application/json")
	public CalculationResult runScript(/* @ModelAttribute SourceDataDTO sourceData,*/ @RequestParam(name = "script") String scriptName, /*@RequestBody ParametersDTO params*/@RequestBody RunDataDTO runData)
			throws Exception {
		System.out.println("Unmodified headers:\n" + runData.getParameters().getHeaders());
		//System.out.println("Unmodified input:\n" + dataAsCSVString);
		String dataAsCSVString = runData.getData();
		if (dataAsCSVString.isEmpty()) {
			return null;
		}
		Matcher m = allowedParameterPattern.matcher(dataAsCSVString);
		
		if(m.find()) {
			System.err.println("Found unallowed character in data values:"+dataAsCSVString.substring(m.start(),m.end()));
			throw new IllegalArgumentException("Found unallowed character in data values: "+dataAsCSVString.substring(m.start(),m.end()));
		}
		dataAsCSVString = dataAsCSVString.replaceAll("\n;+", "\n");
		dataAsCSVString = dataAsCSVString.replaceAll("\n\n+;", "\n");
		dataAsCSVString = dataAsCSVString.replaceAll(";\n+", "\n");
		//dataAsCSVString = dataAsCSVString.replaceAll("\n\n", "\n");
		//dataAsCSVString = dataAsCSVString.replaceAll("\n\n", "\n");
		if (dataAsCSVString.endsWith("\n")) {
			dataAsCSVString = dataAsCSVString.substring(0, dataAsCSVString.length() - 2);
		}
		//if (dataAsCSVString.endsWith("\n;")) {
		//	dataAsCSVString = dataAsCSVString.substring(0, dataAsCSVString.length() - 2);
		//}
		dataAsCSVString = dataAsCSVString.replaceAll(",", ".");
		System.out.println(dataAsCSVString);
		System.out.println("EOF");
		String[] lines = dataAsCSVString.split(";\n");
		if (lines[0].isEmpty()) {
			return null;
		}

		String[] firstLine = lines[0].split(";");
		String[][] data = new String[lines.length][firstLine.length];
		for (int i = 0; i < lines.length; i++) {
			String[] nextDataLine = lines[i].split(";");
			if (nextDataLine.length != firstLine.length) {
				continue;
			}
			for (int j = 0; j < nextDataLine.length; j++) {
				data[i][j] = nextDataLine[j];
			}
		}
		System.out.println("data:");
		printData(data);
		SourceDataDTO dataDTO = new SourceDataDTO();
		dataDTO.setData(data);
		dataDTO.setDataAsString(dataAsCSVString);
		//sourceData.setData(data);
		//sourceData.setDataAsString(dataAsCSVString);
		//System.out.println("saved data:");
		//printData(sourceData.getData());
		//System.out.println("We run the script with the following data:");
		//printData(dataAsCSVString);
		System.out.println("Running Script:" + scriptName + " With params=");
		//if (sourceData.getData() == null) {
		//	throw new Exception("No Data set");
		//}
		
		for (ParamDTO param : runData.getParameters().getParams()) {
			m = allowedParameterPattern.matcher(param.getValue());
			if(m.find()) {
				System.err.println("Found unallowed character in param value: "+param.getValue().substring(m.start(),m.end()));
				throw new IllegalArgumentException("Found unallowed character in param value: "+param.getValue().substring(m.start(),m.end()));
			}
			m = allowedParameterPattern.matcher(param.getName());
			if(m.find()) {
				System.err.println("Found unallowed character in param name: "+param.getName().substring(m.start(),m.end()));
				throw new IllegalArgumentException("Found unallowed character in param name: "+param.getName().substring(m.start(),m.end()));
			}
			System.out.println("Name=" + param.getName() + " Value=" + param.getValue());
		}
		return PythonFactory.executePythonScript("countingprocedures",scriptName, runData.getParameters(), dataDTO);
	}

	/*
	 * We transfer the data as plain text so that it consumes less bandwidth than
	 * JSON format
	 */
	//@CrossOrigin(origins = "http://127.0.0.1:8081")
	//@CrossOrigin
	//@RequestMapping(value = "/sourcedata", method = RequestMethod.POST, consumes = "text/plain")
	//public void setData(/*@ModelAttribute SourceDataDTO sourceData,*/ @RequestParam(name = "headers") String headers, @RequestBody String dataAsCSVString) {
		/*System.out.println("Unmodified headers:\n" + headers);
		//System.out.println("Unmodified input:\n" + dataAsCSVString);
		if (dataAsCSVString.isEmpty()) {
			return;
		}
		dataAsCSVString = dataAsCSVString.replaceAll("\n;+", "\n");
		dataAsCSVString = dataAsCSVString.replaceAll("\n\n+;", "\n");
		//dataAsCSVString = dataAsCSVString.replaceAll("\n\n", "\n");
		//dataAsCSVString = dataAsCSVString.replaceAll("\n\n", "\n");
		if (dataAsCSVString.endsWith("\n")) {
			dataAsCSVString = dataAsCSVString.substring(0, dataAsCSVString.length() - 2);
		}
		//if (dataAsCSVString.endsWith("\n;")) {
		//	dataAsCSVString = dataAsCSVString.substring(0, dataAsCSVString.length() - 2);
		//}
		dataAsCSVString = dataAsCSVString.replaceAll(",", ".");
		System.out.println(dataAsCSVString);
		System.out.println("EOF");
		String[] lines = dataAsCSVString.split(";\n");
		if (lines[0].isEmpty()) {
			return;
		}

		String[] firstLine = lines[0].split(";");
		String[][] data = new String[lines.length][firstLine.length];
		for (int i = 0; i < lines.length; i++) {
			String[] nextDataLine = lines[i].split(";");
			if (nextDataLine.length != firstLine.length) {
				continue;
			}
			for (int j = 0; j < nextDataLine.length; j++) {
				data[i][j] = nextDataLine[j];
			}
		}
		System.out.println("data before:");
		printData(sourceData.getData());
		sourceData.setData(data);
		sourceData.setDataAsString(dataAsCSVString);
		System.out.println("saved data:");
		printData(sourceData.getData());
	}
	*/

	public static void printData(String[][] data) {
		System.out.println("Printing Data:");
		if (data == null) {
			System.out.println("The data is null");
		} else {
			for (int i = 0; i < data.length; i++) {
				for (int j = 0; j < data[i].length; j++) {
					System.out.print(data[i][j] + ";");
				}
				System.out.println();
			}
		}
		System.out.println("End of Data");
	}

}
