package kib.cp.restcontroller;

import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import kib.cp.dtos.ExampleDTO;
import kib.cp.dtos.ExamplesDTO;
import kib.cp.factories.ExampleFactory;

@RestController
public class ExampleDataController {

	//@CrossOrigin(origins = "http://127.0.0.1:8081")
	@CrossOrigin
	@RequestMapping("/examples")
	public ExamplesDTO getExamples() throws Exception {
		Resource[] examples = ExampleFactory.listAllExampleData();
		ExamplesDTO examplesDTO = new ExamplesDTO();
		for(Resource resource:examples) {
			ExampleDTO dto = new ExampleDTO();
			dto.setName(resource.getFilename().replaceAll(".csv", ""));
			dto.setFile(resource.getFilename());
			examplesDTO.getExamples().add(dto);
		}
		return examplesDTO;
	}
	
	//@CrossOrigin(origins = "http://127.0.0.1:8081")
	@CrossOrigin
	@RequestMapping(produces = "text/plain", value = "/example")
	public String loadExampleData(@RequestParam(name = "example") String exampleName) throws Exception {
		System.out.println("Loading exampleData: "+exampleName);
		return ExampleFactory.getExampleData(exampleName);
	}
}
