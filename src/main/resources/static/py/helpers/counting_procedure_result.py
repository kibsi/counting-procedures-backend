#!/usr/bin/env python
# -*- coding: utf-8 -*- 
#@license
#Copyright (c) 2018 Bernhard Kinast. All rights reserved.
#Except where otherwise noted, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
#To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
#or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
class CountingProcedureResult:
	
	def __init__(self):
		self.calculationClasses = []
		self.classCrossings = []
		self.usedIndizes = []
		self.rangeCounts = []
		self.pointsOfInterest = [[]]
		self.P=[x[:] for x in [[0.0] * 1] * 1]
		self.gridFromTo = [x[:] for x in [[0]*1]*1]
		
	def getCalculationClasses(self):
		return self.calculationClasses
	
	def setCalculationClasses(self,calculationClasses):
		self.calculationClasses=calculationClasses
		
	def getClassCrossings(self):
		return self.classCrossings
	
	def setClassCrossings(self,classCrossings):
		self.classCrossings=classCrossings
		
	def getRangeCounts(self):
		return self.rangeCounts
	
	def setRangeCounts(self,rangeCounts):
		self.rangeCounts=rangeCounts
		
	def setMarkovProbabilities(self,P):
		self.P=P
		
	def getMarkovProbabilities(self):
		return self.P
	
	def setUsedIndizes(self,usedIndizes):
		self.usedIndizes=usedIndizes
	
	def getUsedIndizes(self):
		return self.usedIndizes
	
	def getPointsOfInterest(self):
		return self.pointsOfInterest
	
	def setPointsOfInterest(self,pointsOfInterest):
		self.pointsOfInterest=pointsOfInterest
		
	def getGridFromTo(self):
		return self.gridFromTo
	
	def setGridFromTo(self,gridFromTo):
		self.gridFromTo=gridFromTo
		
class CountingProcedureClass:
	def __init__(self):
		self.min = 0
		self.max = 0
		
	def setMin(self,min):
		self.min=min
		
	def getMin(self):
		return self.min
	
	def setMax(self,max):
		self.max=max
		
	def getMax(self):
		return self.max
	
	def getRange(self):
		return self.max-self.min