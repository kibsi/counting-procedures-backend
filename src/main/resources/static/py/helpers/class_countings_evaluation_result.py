#!/usr/bin/env python
# -*- coding: utf-8 -*- 
#@license
#Copyright (c) 2018 Bernhard Kinast. All rights reserved.
#Except where otherwise noted, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
#To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
#or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
class ClassCountingsEvaluationResult:
	
	def __init__(self):
		self.levels = []
		self.tensions = []
		self.sumAmounts = []
		self.levelAmounts = []
		self.maxTensions = []
		self.tolerableNumberOfCycles = []
		self.damageParts = []
		self.totalDamage = 0
		self.percentDamage = []
		#self.pointsOfInterest = [[]]
	
	def getLevels(self):
		return self.levels
	
	def setLevels(self,levels):
		self.levels = levels
		
	def getTensions(self):
		return self.tensions
	
	def setTensions(self,tensions):
		self.tensions=tensions
	
	def getSumAmounts(self):
		return self.sumAmounts
	
	def setSumAmounts(self,sumAmount):
		self.sumAmounts=sumAmounts
		
	def getLevelAmounts(self):
		return self.levelAmounts
	
	def setLevelAmounts(self,levelAmount):
		self.levelAmounts = levelAmounts
		
	def getMaxTensions(self):
		return self.maxTensions
	
	def setMaxTensions(self,maxTensions):
		self.maxTensions=maxTensions
		
	def getTolerableNumberOfCycles(self):
		return self.tolerableNumberOfCycles
	
	def setTolerableNumberOfCycles(self,tolerableNumberOfCycles):
		self.tolerableNumberOfCycles=tolerableNumberOfCycles
	
	def getDamageParts(self):
		return self.damageParts
	
	def setDamageParts(self,damageParts):
		self.damageParts=damageParts
		
	def getTotalDamage(self):
		return self.totalDamage
	
	def setTotalDamage(self,totalDamage):
		self.totalDamage=totalDamage
		
	def getPercentDamage(self):
		return self.percentDamage
	
	def setPercentDamage(self,percentDamage):
		self.percentDamage=percentDamage
		
	#def getPointsOfInterest(self):
	#	return self.pointsOfInterest
	
	#def setPointsOfInterest(self,pointsOfInterest):
	#	self.pointsOfInterest=pointsOfInterest
	