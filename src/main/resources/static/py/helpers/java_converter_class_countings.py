#!/usr/bin/env python
# -*- coding: utf-8 -*- 
#@license
#Copyright (c) 2018 Bernhard Kinast. All rights reserved.
#Except where otherwise noted, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
#To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
#or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.- 
from kib.cp.py import ClassCountingsEvaluationResultDTO
##from helpers.counting_procedure_result import CountingProcedureResult, CountingProcedureClass
from helpers.class_countings_evaluation_result import ClassCountingsEvaluationResult
def convertToJava(pyResult):
    
    if isinstance(pyResult, ClassCountingsEvaluationResultDTO):
        print("result is already of type ClassCountingsEvaluationResultDTO")
        return pyResult
    
    javaResult = ClassCountingsEvaluationResultDTO()
    print("new ClassCountingsEvaluationResultDTO created")
    javaResult.setLevels(pyResult.getLevels())
    javaResult.setTensions(pyResult.getTensions())
    javaResult.setSumAmounts(pyResult.getSumAmounts())
    javaResult.setLevelAmounts(pyResult.getLevelAmounts())
    javaResult.setMaxTensions(pyResult.getMaxTensions())
    javaResult.setTolerableNumberOfCycles(pyResult.getTolerableNumberOfCycles())
    javaResult.setDamageParts(pyResult.getDamageParts())
    javaResult.setTotalDamage(pyResult.getTotalDamage())
    javaResult.setPercentDamage(pyResult.getPercentDamage())
    return javaResult
