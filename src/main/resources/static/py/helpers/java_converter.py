#!/usr/bin/env python
# -*- coding: utf-8 -*- 
#@license
#Copyright (c) 2018 Bernhard Kinast. All rights reserved.
#Except where otherwise noted, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
#To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
#or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
from kib.cp.py import CalculationResult, CalculationClass
##, ClassCountingsEvaluationResultDTO
from helpers.counting_procedure_result import CountingProcedureResult, CountingProcedureClass
##from helpers.class_countings_evaluation_result import ClassCountingsEvaluationResult
def convertToJava(pyResult):
    
    if isinstance(pyResult, CalculationResult):
        print("result is already of type CalculationResult")
        return pyResult
    
    javaResult = CalculationResult()
    print("new CalculationResult created")
    for calcClass in pyResult.getCalculationClasses():
        nextClass = CalculationClass(calcClass.getMin(),calcClass.getMax())
        javaResult.getCalculationClasses().add(nextClass)
    print("CalculationClases created")
    javaResult.setClassCrossings(pyResult.getClassCrossings())
    print("setClassCrossings")
    javaResult.setRangeCounts(pyResult.getRangeCounts())
    print("setRangeCounts")
    javaResult.setMarkovProbabilities(pyResult.getMarkovProbabilities())
    print("setMarkovProbabilities")
    javaResult.setGridFromTo(pyResult.getGridFromTo())
    print("setGridFromTo")
    javaResult.setUsedIndizes(pyResult.getUsedIndizes())
    print("setUsedIndizes")
    javaResult.setPointsOfInterest(pyResult.getPointsOfInterest())
    print("setPointsOfInterest")
    return javaResult
