#!/usr/bin/env python
# -*- coding: utf-8 -*- 
#@license
#Copyright (c) 2018 Bernhard Kinast. All rights reserved.
#Except where otherwise noted, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
#To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
#or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

from helpers.class_countings_evaluation_result import ClassCountingsEvaluationResult

def calculate(data=["wert"],sigma=125,N=2e6,k=7):
    """Miner original
    """
    print("Starting evaluation Miner Elementar")
    print("value of sigma: %s of type %s" % (sigma, type(sigma)))
    print("value of N: %s of type %s" % (N, type(N)))
    print("value of k: %s of type %s" % (k, type(k)))
    result = ClassCountingsEvaluationResult()
    result.setTotalDamage(0)
    amountOfClasses = len(data)
    Hai = 0
    for i in range(amountOfClasses-1,-1,-1):
        listPos = amountOfClasses-1-i
        result.getLevels().append(i+1)
        Sai = abs(data[listPos][2])
        result.getTensions().append(Sai)
        previousHai =Hai
        Hai = data[listPos][3]
        result.getSumAmounts().append(Hai)
        hai = result.getSumAmounts()[listPos]-previousHai
        result.getLevelAmounts().append(hai)
        ##result.getMaxTensions()[i] = sigma*()
        if Sai <= sigma:
            Ni = 0
            Di = 0
        else:
            Ni = N * (Sai/sigma)**(-k)
            Di = hai/Ni
        result.getTolerableNumberOfCycles().append(Ni)
        #print("value of damagePart: %s of type %s" % (damagePart, type(damagePart)))
        result.getDamageParts().append(Di)
        #print("value of result.getTolerableNumberOfCycles()[i]: %s of type %s" % (result.getTolerableNumberOfCycles()[listPos], type(result.getTolerableNumberOfCycles()[listPos])))
        #print("value of result.getLevelAmounts()[i]: %s of type %s" % (result.getLevelAmounts()[listPos], type(result.getLevelAmounts()[listPos])))
        #print("value of result.getTotalDamage(): %s of type %s" % (result.getTotalDamage(), type(result.getTotalDamage())))
        #print("value of result.getDamageParts()[i]: %s of type %s" % (result.getDamageParts()[listPos], type(result.getDamageParts()[listPos])))
        result.setTotalDamage(result.getTotalDamage()+Di)
        
    for i in range(0,amountOfClasses):
        if result.getTotalDamage()>0:
            result.getPercentDamage().append(result.getDamageParts()[i]*100/result.getTotalDamage())
        else:
            result.getPercentDamage().append(0)
        
    return result

    