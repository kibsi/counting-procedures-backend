#!/usr/bin/env python
# -*- coding: utf-8 -*- 
#@license
#Copyright (c) 2018 Bernhard Kinast. All rights reserved.
#Except where otherwise noted, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
#To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
#or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
from helpers.counting_procedure_result import CountingProcedureResult, CountingProcedureClass

def calculate(data=["wert"],classWidth=20):
    """Die Bereichszählung ist ein einparametriges Zählverfahren
        this version counts the positive flanks
    """
    print("Starting counting procedure klassengrenzen_rueckstellbreite")
    print("value of classWidth: %s of type %s" % (classWidth, type(classWidth)))
    result = CountingProcedureResult()
    ranges = [0]*1
    #We start after we found the first maximum
    resetWidth=classWidth
    usedIndizes = result.getUsedIndizes()
    pointsOfInterest = result.getPointsOfInterest()
    foundFirstmaximum = False
    lastDataPoint = data[0][0]
    lastMinimum = data[0][0]
    lastMinimumIndex = 0
    lastMaximumIndex = 0
    crossings = 0
    index=0
    for dataLine in data:
        dataPoint = dataLine[0]
        upperBound = lastDataPoint + resetWidth
        lowerBound = lastDataPoint - resetWidth
        if(dataPoint>lastDataPoint):
            #By using // we get an integer division (or floor function)
            crossings = int((dataPoint-lastMinimum)//classWidth)
            lastDataPoint = dataPoint
            lastMaximumIndex = index
        elif(dataPoint<lastDataPoint):
            if(crossings>0):
                while(len(ranges)<crossings):
                    ranges.append(0)
                for i in range(0,crossings):
                    ranges[i] = ranges[i]+1
                    pointsOfInterest.append([lastMinimumIndex,lastMinimum+(i+1)*classWidth,lastMaximumIndex,lastMinimum+(i+1)*classWidth])
                usedIndizes.append(lastMinimumIndex)
                usedIndizes.append(lastMaximumIndex)
            crossings = 0
            lastDataPoint = dataPoint
            #if(dataPoint<lastMinimum):
            lastMinimum = dataPoint
            lastMinimumIndex = index
        else:
            #Moving downwards but not enough to start counting again
            #We do nothing here, but it helps for understanding to see this case
            pass
        index+=1
    
    for i in range(0,len(ranges)):
        nextClass = CountingProcedureClass()
        nextClass.setMin(0)
        nextClass.setMax((i+1)*classWidth)
        #nextClass = CalculationClass(0,(i+1)*classWidth)
        #result.getCalculationClasses().add(nextClass)
        result.getCalculationClasses().append(nextClass)
    result.setRangeCounts(ranges)
    return result

    