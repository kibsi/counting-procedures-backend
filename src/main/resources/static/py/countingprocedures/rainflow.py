#!/usr/bin/env python
# -*- coding: utf-8 -*- 
#@license
#Copyright (c) 2018 Bernhard Kinast. All rights reserved.
#Except where otherwise noted, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
#To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
#or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
from helpers.counting_procedure_result import CountingProcedureResult, CountingProcedureClass

def calculate(data=["wert"],min=0.0,max=200.0,amount=16,resetWidth=0.0):
    """Die Klassengrenzenüberschreitungszählung ist ein einparametriges Zählverfahren"""
    print("Starting counting procedure klassengrenzen_rueckstellbreite")
    print("value of min: %s of type %s" % (min, type(min)))
    print("value of max: %s of type %s" % (max, type(max)))
    print("value of amount: %s of type %s" % (amount, type(amount)))
    print("value of resetWidth: %s of type %s" % (resetWidth, type(resetWidth)))
    drawPositive = True
    drawNegative = True
    
    result = CountingProcedureResult()
    step = float(max-min)/amount
    print("value of step: %s of type %s" % (step, type(step)))
    #initialize the grid with zeros
    gridFromTo = [x[:] for x in [[0] * amount] * amount]
    result.setGridFromTo(gridFromTo)
    for i in range(0,amount):
        nextClass = CountingProcedureClass()
        nextClass.setMin(i*step+min)
        nextClass.setMax((i+1)*step+min)
        result.getCalculationClasses().append(nextClass)
    calculationClasses = result.getCalculationClasses()
    usedIndizes = result.getUsedIndizes()
    #addedIndex = False
    pointsOfInterest = result.getPointsOfInterest()
    lastMaximumPoint = data[0][0]
    lastMaximumClass = getClassForPoint(lastMaximumPoint,calculationClasses)
    lastMaximumIndex = 0
    lastMinimumPoint = data [0][0]
    lastMinimumClass = getClassForPoint(lastMinimumPoint,calculationClasses)
    lastMinimumIndex = 0
    wasMovingUpwards = False
    dataPoint = data[0][0]
    dataPointClass = getClassForPoint(data[0][0], calculationClasses)
    extrema = []
    classCrossings = [0]*amount
    
    for index, dataline in enumerate(data):
        if dataline[0] < min or dataline[0] >= max:
            #print("Skipping ")
            continue
        previousDataPoint = dataPoint
        dataPoint = dataline[0]
        previousClass = dataPointClass
        previousIndex = index
        dataPointClass = getClassForPoint(dataPoint,calculationClasses)
        print("evaluating index: %s of value %s of type %s dataPointClass %s lastMaximumClass %s lastMinimumClass %s" % (index, dataPoint,type(dataPoint),dataPointClass,lastMaximumClass,lastMinimumClass))
        if dataPointClass > previousClass:
            
            if wasMovingUpwards:
                #just override the previous maximum we found for a higher one!
                pass
            else:
                #before we were moving downwards, so the last minimum counts
                usedIndizes.append(lastMinimumIndex)
                #
                extrema.append([lastMinimumIndex,lastMinimumPoint,lastMinimumClass])
                #pointsOfInterest.append([lastMinimumIndex,lastMinimumPoint,lastMaximumIndex,lastMaximumPoint])
                #
                
                lastMinimumPoint = previousDataPoint
                lastMinimumClass = previousDataPoint
                lastMinimumIndex = previousIndex
            lastMaximumPoint = dataPoint
            lastMaximumClass = dataPointClass
            lastMaximumIndex = index
            wasMovingUpwards = True
            #print("moving upwards maximum at %s minimum  at %s" % (lastMaximumClass,lastMinimumClass))
        elif dataPointClass < previousClass:
            
            if wasMovingUpwards:
                #before we were moving upwards, so the last maximum counts
                usedIndizes.append(lastMaximumIndex)
                extrema.append([lastMaximumIndex,lastMaximumPoint,lastMaximumClass])
                lastMaximumPoint = previousDataPoint
                lastMaximumClass = previousClass
                lastMaximumIndex = previousIndex
            else:
                #just override the previous minimum we found for a lower one!
                pass
            lastMinimumPoint = dataPoint
            lastMinimumClass = dataPointClass
            lastMinimumIndex = index
            wasMovingUpwards = False
            #print("moving downwards maximum at %s minimum  at %s" % (lastMaximumClass,lastMinimumClass))
    #Do not forget about the points in the end
    openMinima = []
    openMaxima = []
    closedMinima = []
    closedMaxima = []
    for i in range(0,len(extrema)):
        extrema[i].append(i)
        if i==0:
            continue
        previousExtremum = extrema[i-1]
        # contains: [indexOfPoint, valueOfPoint, classOfPoint, indexOfExtremum]
        extremum = extrema[i]
        entryToAppend = [previousExtremum,extremum]
        nextComparable = entryToAppend
        #print("--------------------------%s---------------------------" % (nextComparable[0][0]))
        if extremum[2]>previousExtremum[2]:
            #upwards - minimum to maximum
            isCutOff = False
            for openMaximum in reversed(openMaxima):
                minimum = nextComparable[0]
                maximum = nextComparable[-1]
                otherMinimum = openMaximum[0]
                otherMaximum = openMaximum[-1]
                #print("minimum=%s" % minimum)
                #print("maximum=%s" % maximum)
                #print("otherMinimum=%s" % otherMinimum)
                #print("otherMaximum=%s" % otherMaximum)
                #print("openMaximum=%s" % openMaximum)
                #o = openMaximum[-1]
                if otherMaximum[2] > maximum[2]: #or openMaximum[-1][2] <= minimum[2]:
                    #print("Stopped at otherMaximum=%s >= maximum=%s for comparable: %s" % (otherMaximum,maximum,nextComparable))
                    break
                #if otherMaximum[2] == maximum[2]:
                #    nextComparable = openMaximum
                #    continue
                if otherMinimum[2] < minimum[2] and otherMaximum[2] > minimum[2]:
                    if otherMaximum[2] == maximum[2]:
                        pass
                    else:
                        nextComparable.append(openMaximum[-1])
                        openMaximum.append(nextComparable[-2])
                    #REMOVE OPENMAXIMUM
                    #
                    #print("Exchanging %s <=> %s" % (maximum[0],otherMaximum[0]))
                    if nextComparable in openMaxima:
                        openMaxima.remove(nextComparable)
                        closedMaxima.append(nextComparable)
                        #print("Line %s is removed" % (nextComparable))
                    nextComparable = openMaximum
                    
                    isCutOff = True
            if isCutOff:
                closedMaxima.append(entryToAppend)
                #print("Line %s is removed" % (entryToAppend))
            else:
                openMaxima.append(entryToAppend)
        elif extremum[2]<previousExtremum[2]:
            #upwards - minimum to maximum
            isCutOff = False
            for openMinimum in reversed(openMinima):
                minimum = nextComparable[-1]
                maximum = nextComparable[0]
                otherMinimum = openMinimum[-1]
                otherMaximum = openMinimum[0]
                #print("minimum=%s" % minimum)
                #print("maximum=%s" % maximum)
                #print("otherMinimum=%s" % otherMinimum)
                #print("otherMaximum=%s" % otherMaximum)
                #print("openMinimum=%s" % openMinimum)
                #o = openMaximum[-1]
                if otherMinimum[2] < minimum[2]:
                    #print("Stopped at otherMinimum=%s >= minimum=%s for comparable: %s" % (otherMinimum,minimum,nextComparable))
                    break
                #if otherMinimum[2] == minimum[2]:
                #    nextComparable = openMinimum
                #    continue
                #print("otherMinimum=%s < minimum=%s and maximum=%s > otherMaximum=%s" % (otherMinimum[2],minimum[2],maximum[2],otherMaximum[2]))
                #if otherMinimum[2] < minimum[2] and otherMaximum[2]<maximum[2] and otherMaximum[2] > minimum[2]:
                #print("Check: %s" % entryToAppend)
                if otherMaximum[2] > maximum[2]  and otherMinimum[2] < maximum[2]: #and otherMaximum[2] > minimum[2]:#and otherMaximum[2]<maximum[2]
                    #print("Hmmm: %s" % entryToAppend)
                    if otherMinimum[2] == minimum[2]:
                        pass
                    else:
                        nextComparable.append(openMinimum[-1])
                        openMinimum.append(nextComparable[-2])
                    #REMOVE OPENMAXIMUM
                    #
                    #print("Exchanging %s <=> %s" % (maximum[0],otherMaximum[0]))
                    if nextComparable in openMinima:
                        
                        openMinima.remove(nextComparable)
                        closedMinima.append(nextComparable)
                        #print("Line %s is removed" % (nextComparable))
                    nextComparable = openMinimum
                    isCutOff = True        
            if isCutOff:
                closedMinima.append(entryToAppend)
                #print("Line %s is removed" % (entryToAppend))
            else:
                #print("openMinima appended %s" % entryToAppend)
                openMinima.append(entryToAppend)
                #print("closedMinima appended %s" % entryToAppend)
        #pointsOfInterest.append([previousExtremum[0],previousExtremum[1],extremum[0],extremum[1]])
    #result.setClassCrossings(classCrossings)
    #for entry in usedIndizes:
        #print("Interesting point at: %s of type %s" % (entry, type(entry)))
    #print("openMaxima: %s"%"\n".join([str(row) for row in openMaxima]))
    #print("closedMaxima: %s"%"\n".join([str(row) for row in closedMaxima]))
    #print("openMinima: %s"%"\n".join([str(row) for row in openMinima]))
    #print("closedMinima: %s"%"\n".join([str(row) for row in closedMinima]))
    closedMaxima.extend(openMaxima)
    #print("unSortedClosedMaxima: %s"%"\n".join([str(row) for row in closedMaxima]))
    closedMaxima = sorted(closedMaxima, key = lambda maxRef:maxRef[0])
    closedMinima.extend(openMinima)
    #print("unSortedClosedMinima: %s"%"\n".join([str(row) for row in closedMinima]))
    closedMinima = sorted(closedMinima, key = lambda minRef:minRef[0])
    
    #print("sortedClosedMaxima: %s"%"\n".join([str(row) for row in closedMaxima]))
    #print("sortedClosedMinima: %s"%"\n".join([str(row) for row in closedMinima]))
    for minToMax in closedMaxima:
        gridFromTo[minToMax[0][2]][minToMax[-1][2]] += 1
        #print("[%s][%s] -> %s" % (minToMax[0][2],minToMax[-1][2],minToMax))
    for maxToMin in closedMinima:
        gridFromTo[maxToMin[0][2]][maxToMin[-1][2]] += 1
        #print("[%s][%s] -> %s" % (maxToMin[0][2],maxToMin[-1][2],maxToMin))
    #print("gridFromTo:")
    
    #print("     TO\t%s" % "\t".join([str(i) for i in range(len(gridFromTo))]))
    #print("FROM\t%s" % "".join(["________" for i in range(len(gridFromTo))]))
    #for index,row in enumerate(gridFromTo):
        #print("%s:\t%s" % (index,"\t".join([str(x) for x in row])))
    
    #The following code is only for printing the lines to have a visual feedback
    for entry in closedMaxima:
        #print("Maximum entry at: %s" % (entry))
        if not drawPositive:
            break
        l = len(entry)
        i = 0
        while i < l-1:
            
            point = entry[i]
            pointIndex = point[0]
            pointValue = point[1]
            pointClass = point[2]
            
            # Skipp unneded entries
            while (i < l-3) and entry[i+2][2] < entry[i+1][2]:
                #print("Skipping index %s and %s" % (i+1,i+2))
                i=i+2
            
            for j in range(i+2,l):
                #print("Checking entry[%s]=%s < entry[%s]=%s" % (j,entry[j][1],i+1,entry[i+1][1]))
                if entry[j][2] < entry[i+1][2] and entry[j][0] < entry[i+1][0]:
                    #print("Found new smaller index, skipping from index %s to index %s" % (i,j-1))
                    i=j-1
                    
            nextPoint = entry[i+1]
            nextPointIndex = nextPoint[0]
            nextPointValue = nextPoint[1]
            nextPointClass = nextPoint[2]
            isStoppedByOther = False
            if i >= l-3:
                #print("Checking if last value is lower than pre-last value")
                lastPoint = entry[-1]
                lastPointIndex = lastPoint[0]
                lastPointValue = lastPoint[1]
                lastPointClass = lastPoint[2]
                
                
                if lastPointClass < entry[-2][2]:
                    #print("Last point is lower than pre-last point - It is a stopped line")
                    isStoppedByOther = True
  
            if isStoppedByOther:
                #print("printing reduced line %s" % ([pointIndex,pointValue,pointIndex,lastPointValue]))
                pointsOfInterest.append([pointIndex,pointValue,pointIndex,lastPointValue-step*0.1])
                pointsOfInterest.append([pointIndex-0.5,lastPointValue-step*0.1,pointIndex+0.5,lastPointValue-step*0.1])
            else:
                if nextPointIndex > pointIndex:
                    
                    #print("printing normal line %s" % ([pointIndex,pointValue,nextPointIndex,nextPointValue]))
                    pointsOfInterest.append([pointIndex,pointValue,nextPointIndex,nextPointValue])
            i += 1
        #print("entry %s" % (entry))
        indexOfNextMinimum = entry[-1][3]+1
        drawDown = True
        for entryPart in entry:
            if entryPart[2] > entry[-1][2]:
                drawDown = False
                break
        if not drawDown:
            continue
        #print("a = %s of type %s" % (a,type(a)))
        l = len(extrema)
        #print("b = %s of type %s" % (b,type(b)))
        #indexOfNextMinimum = a if a<b else b
        if indexOfNextMinimum < l:
            nextMinimum = extrema[indexOfNextMinimum]
            pointsOfInterest.append([nextPointIndex,nextPointValue,nextMinimum[0],nextPointValue])
        else:
            pointsOfInterest.append([nextPointIndex,nextPointValue,len(data)-1,nextPointValue])
    
    for entry in closedMinima:
        #print("Minimum entry at: %s" % (entry))
        if not drawNegative:
            break
        l = len(entry)
        i = 0
        while i < l-1:
            
            point = entry[i]
            pointIndex = point[0]
            pointValue = point[1]
            pointClass = point[2]
            
            # Skipp unneded entries
            while (i < l-3) and entry[i+2][2] > entry[i+1][2]:
                #print("Skipping index %s and %s" % (i+1,i+2))
                i=i+2
            
            for j in range(i+2,l):
                #print("Checking entry[%s]=%s > entry[%s]=%s" % (j,entry[j][1],i+1,entry[i+1][1]))
                if entry[j][2] > entry[i+1][2] and entry[j][0] < entry[i+1][0]: #entry[j][1] > entry[i+1][1] and
                    #print("Found new bigger index, skipping from index %s to index %s" % (i,j-1))
                    i=j-1
                    
            nextPoint = entry[i+1]
            nextPointIndex = nextPoint[0]
            nextPointValue = nextPoint[1]
            nextPointClass = nextPoint[2]
            isStoppedByOther = False
            if i >= l-3:
                #print("Checking if last value is bigger than pre-last value")
                lastPoint = entry[-1]
                lastPointIndex = lastPoint[0]
                lastPointValue = lastPoint[1]
                lastPointClass = lastPoint[2]
                
                
                if lastPointClass > entry[-2][2]:
                    #print("Last point is bigger than pre-last point - It is a stopped line")
                    isStoppedByOther = True
  
            if isStoppedByOther:
                #print("printing reduced line %s" % ([pointIndex,pointValue,pointIndex,lastPointValue]))
                pointsOfInterest.append([pointIndex,pointValue,pointIndex,lastPointValue-step*0.1])
                pointsOfInterest.append([pointIndex-0.5,lastPointValue-step*0.1,pointIndex+0.5,lastPointValue-step*0.1])
            else:
                if nextPointIndex > pointIndex:
                    
                    #print("printing normal line %s" % ([pointIndex,pointValue,nextPointIndex,nextPointValue]))
                    pointsOfInterest.append([pointIndex,pointValue,nextPointIndex,nextPointValue])
            i += 1
        #print("entry %s" % (entry))
        indexOfNextMinimum = entry[-1][3]+1
        drawUp = True
        for entryPart in entry:
            if entryPart[2] < entry[-1][2]:
                drawUp = False
                break
        if not drawUp:
            continue
        #print("a = %s of type %s" % (a,type(a)))
        l = len(extrema)
        #print("b = %s of type %s" % (b,type(b)))
        #indexOfNextMinimum = a if a<b else b
        if indexOfNextMinimum < l:
            nextMinimum = extrema[indexOfNextMinimum]
            pointsOfInterest.append([nextPointIndex,nextPointValue,nextMinimum[0],nextPointValue])
        else:
            pointsOfInterest.append([nextPointIndex,nextPointValue,len(data)-1,nextPointValue])
    return result

def getClassForPoint(dataPoint,calculationClasses):
    index = 0
    for calculationClass in calculationClasses:
        if (dataPoint >= calculationClass.getMin()) and (dataPoint < calculationClass.getMax()):
            return index
        index += 1
        