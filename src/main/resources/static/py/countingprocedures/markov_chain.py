#!/usr/bin/env python
# -*- coding: utf-8 -*- 
#@license
#Copyright (c) 2018 Bernhard Kinast. All rights reserved.
#Except where otherwise noted, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
#To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
#or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
from helpers.counting_procedure_result import CountingProcedureResult, CountingProcedureClass

def calculate(data=["value"],min=0.0,max=200.0,amount=16):
    """The Markov Chain is a simple yet intuitive approach"""
    print("Starting counting procedure markov-chain")
    print("value of min: %s of type %s" % (min, type(min)))
    print("value of max: %s of type %s" % (max, type(max)))
    print("value of amount: %s of type %s" % (amount, type(amount)))
    result = CountingProcedureResult()
    
    step = float(max-min)/amount
    #resetWidth = step
    for i in range(0,amount):
        nextClass = CountingProcedureClass()
        nextClass.setMin(i*step+min)
        nextClass.setMax((i+1)*step+min)
        result.getCalculationClasses().append(nextClass)
    calculationClasses = result.getCalculationClasses()
    
    #The probability matrix
    #initialize the matrix with 0
    P = [x[:] for x in [[0] * amount] * amount]
    
    #classCrossings = [0]*amount
    #foundFirstmaximum = False
    lastClassIndex = 0;
    nextClassIndex = 0;
    currentClassIndex = 0;
    for row in data:
        nextValue = row[0]
        for i in range (amount-1,-1,-1):
            if calculationClasses[i].getMin() <= nextValue:
                nextClassIndex = i
                break
            
        if nextClassIndex > currentClassIndex:
            if currentClassIndex>lastClassIndex:
                #We still move upwards
                pass
            elif currentClassIndex<lastClassIndex:
                #direction changed to downwards, enter the last known class before continue
                P[lastClassIndex][currentClassIndex] = P[lastClassIndex][currentClassIndex] +1;
                
                lastClassIndex = currentClassIndex
            else:
                #We are in the same class
                pass
        if nextClassIndex < currentClassIndex:
            if currentClassIndex>lastClassIndex:
                #direction changed to upwards, enter the last known class before continue
                P[lastClassIndex][currentClassIndex] = P[lastClassIndex][currentClassIndex] +1;
                
                lastClassIndex = currentClassIndex
            elif currentClassIndex<lastClassIndex:
                #We still move downwards
                pass
            else:
                #We are in the same class
                pass
        currentClassIndex = nextClassIndex
    for i in range(0,amount):
        for j in range(0,amount):
            #value = P[i,j]
            print("P[%s][%s]=%s" % (i,j,P[i][j]))
    # Deep Copy of P
    gridFromTo = [[cell for cell in row] for row in P]
    result.setGridFromTo(gridFromTo);
    #for i in range(0,amount):
    #    row = P[i]
    #    #col = [row[i] for row in P];
    #    rowSum = float(sum(row))
    #    if rowSum > 0:
    #        for j in range(0,amount):
    #            P[i][j] = P[i][j]/rowSum
    #        #col[:] = [entry/colSum for entry in col];
    #for i in range(0,amount):
    #    for j in range(0,amount):
    #        print("P[%s][%s]=%s" % (i,j,P[i][j]))
    #result.setMarkovProbabilities(P)
    
    return result


    