#!/usr/bin/env python
# -*- coding: utf-8 -*- 
#@license
#Copyright (c) 2018 Bernhard Kinast. All rights reserved.
#Except where otherwise noted, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
#To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
#or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
from helpers.counting_procedure_result import CountingProcedureResult, CountingProcedureClass

def calculate(data=["value"],min=0.0,max=200.0,amount=16):
    """Die Klassengrenzenüberschreitungszählung ist ein einparametriges Zählverfahren"""
    print("Starting counting procedure klassengrenzen_rueckstellbreite")
    print("value of min: %s of type %s" % (min, type(min)))
    print("value of max: %s of type %s" % (max, type(max)))
    print("value of amount: %s of type %s" % (amount, type(amount)))
    result = CountingProcedureResult()
    step = float(max-min)/amount
    for i in range(0,amount):
        nextClass = CountingProcedureClass()
        nextClass.setMin(i*step+min)
        nextClass.setMax((i+1)*step+min)
        result.getCalculationClasses().append(nextClass)
    calculationClasses = result.getCalculationClasses()
    #usedIndizes = result.getUsedIndizes()
    #addedIndex = False
    pointsOfInterest = result.getPointsOfInterest()
    usedIndizes = result.getUsedIndizes();
    classCrossings = [0]*amount
    
    lastDataPoint = data[0][0]
    lastDataPointClass = getDataClassForPoint(lastDataPoint, calculationClasses)
    lastIndex = 0
    wasMovingUpwards = True
    wasMovingDownwards = False
    
    lastMaximum = data[0][0]
    lastMaximumClass = getDataClassForPoint(lastMaximum, calculationClasses)
    lastMaximumIndex = 0
    
    lastMinimum = data[0][0]
    lastMinimumClass = getDataClassForPoint(lastMinimum, calculationClasses)
    lastMinimumIndex = 0
    
    #index = 0
    for index, dataRow in enumerate(data):
        dataPoint = dataRow[0]
        dataPointClass = getDataClassForPoint(dataPoint, calculationClasses)
        if dataPointClass.getMax() > lastDataPointClass.getMax():
            isMovingUpwards = True
            isMovingDownwards = False
        elif dataPointClass.getMax() < lastDataPointClass.getMax():
            isMovingUpwards = False
            isMovingDownwards = True
        else:
            # if the dataPoint has the same value as the last, skip it
            continue
        # if isMovingDownwards and wasMovingUpwards the last dataPoint was a local Maximum
        if isMovingDownwards and wasMovingUpwards:
            usedIndizes.append(lastIndex)
            for i in range(0,amount):
                classToCheck = calculationClasses[i]
                if classToCheck.getMin() >= lastMinimumClass.getMax() and classToCheck.getMax() <= lastDataPointClass.getMax():
                    classCrossings[i] += 1
                    pointsOfInterest.append([lastMinimumIndex,classToCheck.getMin(),lastIndex,classToCheck.getMin()])
            lastMaximum = lastDataPoint
            lastMaximumClass = getDataClassForPoint(lastMaximum, calculationClasses)
            lastMaximumIndex = lastIndex
            
        if isMovingUpwards and wasMovingDownwards:
            usedIndizes.append(lastIndex)
            lastMinimum = lastDataPoint
            lastMinimumClass = getDataClassForPoint(lastMinimum, calculationClasses)
            lastMinimumIndex = lastIndex
        
        wasMovingUpwards = isMovingUpwards
        wasMovingDownwards = isMovingDownwards
        lastDataPoint = dataPoint;
        lastDataPointClass = dataPointClass;
        lastIndex = index
    result.setClassCrossings(classCrossings)
    return result

def getDataClassForPoint(dataPoint, dataClasses):
    for dataClass in dataClasses:
        if dataClass.getMin() <= dataPoint and dataClass.getMax() > dataPoint:
            return dataClass
    if dataPoint < dataClasses[0].getMin():
        return dataClasses[0]
    else:
        return dataClasses[-1]

    