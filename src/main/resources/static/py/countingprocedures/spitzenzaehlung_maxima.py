#!/usr/bin/env python
# -*- coding: utf-8 -*- 
#@license
#Copyright (c) 2018 Bernhard Kinast. All rights reserved.
#Except where otherwise noted, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
#To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
#or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
from helpers.counting_procedure_result import CountingProcedureResult, CountingProcedureClass

def calculate(data=["wert"],minimum=0.0,maximum=200.0,amountOfClasses=16,resetWidth=0):
    """
    (de)
    Die Spitzenzählung ist ein einparametriges Zählverfahren
    (en)
    peak-counting is a one-dimensional counting procedure
    """
    print("Starting counting procedure peak_counting_maxima")
    print("value of min: %s of type %s" % (minimum, type(minimum)))
    print("value of max: %s of type %s" % (maximum, type(maximum)))
    print("value of amountOfClasses: %s of type %s" % (amountOfClasses, type(amountOfClasses)))
    print("value of resetWidth: %s of type %s" % (resetWidth, type(resetWidth)))
    result = CountingProcedureResult()
    classWidth = float(maximum-minimum)/amountOfClasses
    for i in range(0,amountOfClasses):
        nextClass = CountingProcedureClass()
        nextClass.setMin(i*classWidth+minimum)
        nextClass.setMax((i+1)*classWidth+minimum)
        result.getCalculationClasses().append(nextClass)
    calculationClasses = result.getCalculationClasses()
    usedIndizes = result.getUsedIndizes()
    pointsOfInterest = result.getPointsOfInterest()
    classCountings = [0]*amountOfClasses
    isUpwardsMovement = False
    lastDataPoint = data[0][0]
    lastMinimum =  data[0][0]
    lastMaximum = data[0][0]
    preLastMaximum = data[0][0]
    lastMaximumIndex = 0
    preLastMaximumIndex=0
    index = 0
    upperBound=data[0][0]
    lowerBound=data[0][0]
    currentResetWidth = 0
    for dataLine in data:
        print("lastDataPoint %s" % (lastDataPoint))
        print("lastMinimum %s" % (lastMinimum))
        print("resetWidth %s" % (resetWidth))
        resetMinimum = (lastMinimum + currentResetWidth)
        print("resetMinimum %s" % (resetMinimum))
        resetMaximum = (lastMaximum - currentResetWidth)
        print("resetMaximum %s" % (resetMaximum))
        lastLowerBound = lowerBound
        lastUpperBound = upperBound
        upperBound = max([lastDataPoint,resetMinimum])
        lowerBound = min([lastDataPoint,resetMaximum])
        #pointsOfInterest.append([index,dataLine[0]])
        if (dataLine[0] > upperBound):
            #the new point is higher than the last one. So the last one was no maxima.
            isUpwardsMovement = True
            #if(dataLine[0]>lastMaximum):
            lastMaximum = dataLine[0]
            lastMaximumIndex = index
            lastMinimum = dataLine[0]
            currentResetWidth=0
        elif (dataLine[0] < lowerBound):
            #the new dataPoint is lower than the last one.
            #If the last movement was upwards, the last point was a maxima because it is going downwards now
            currentResetWidth=0
            if(isUpwardsMovement):
                #We iterate through the classes starting with the lowest one
                for i in range(amountOfClasses):
                    #If the class upper limit is lower
                    print("class %s [%s - %s]" % (i,calculationClasses[i].getMin(),calculationClasses[i].getMax()))
                    if ((lastMaximum <= calculationClasses[i].getMax()) & (lastMaximum >= calculationClasses[i].getMin())):
                        print("point %s <= %s" % (lastMaximum,calculationClasses[i].getMax()))
                        classCountings[i] += 1
                        usedIndizes.append(lastMaximumIndex)
                        pointsOfInterest.append([preLastMaximumIndex,preLastMaximum-resetWidth,lastMaximumIndex,preLastMaximum-resetWidth])
                        pointsOfInterest.append([preLastMaximumIndex,preLastMaximum+resetWidth,lastMaximumIndex,preLastMaximum+resetWidth])
                        preLastMaximumIndex = lastMaximumIndex
                        preLastMaximum = lastMaximum
                        lastMinimum = dataLine[0]
                        lastMaximum = dataLine[0]
                        lastMaximumIndex = index
                        currentResetWidth = resetWidth
                        break
                    #if dataLine[0] > calculationClasses[i].getMin():
            isUpwardsMovement = False
            #if(dataLine[0]<lastMinimum):
            lastMinimum = dataLine[0];
            #Found starting point (first maximum)
        #Always set the last data point. It is needed for the next bounds calculation
        lastDataPoint = dataLine[0]
        #if(index>0):
        #    pointsOfInterest.append([index-1,lastLowerBound,index,lowerBound])
        #    pointsOfInterest.append([index-1,lastUpperBound,index,upperBound])
        index += 1
    #handle the results
    result.setClassCrossings(classCountings)
    return result

    