#!/usr/bin/env python
# -*- coding: utf-8 -*- 
#@license
#Copyright (c) 2018 Bernhard Kinast. All rights reserved.
#Except where otherwise noted, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
#To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
#or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
from helpers.counting_procedure_result import CountingProcedureResult, CountingProcedureClass

def calculate(data=["wert"],min=0.0,max=200.0,amountOfClasses=16,resetWidth=0):
    """
    (de)
    Die Spitzenzählung ist ein einparametriges Zählverfahren
    (en)
    peak-counting is a one-dimensional counting procedure
    """
    print("Starting counting procedure peak_counting_maxima")
    print("value of min: %s of type %s" % (min, type(min)))
    print("value of max: %s of type %s" % (max, type(max)))
    print("value of amountOfClasses: %s of type %s" % (amountOfClasses, type(amountOfClasses)))
    print("value of resetWidth: %s of type %s" % (resetWidth, type(resetWidth)))
    result = CountingProcedureResult()
    classWidth = float(max-min)/amountOfClasses
    for i in range(0,amountOfClasses):
        nextClass = CountingProcedureClass()
        nextClass.setMin(i*classWidth+min)
        nextClass.setMax((i+1)*classWidth+min)
        result.getCalculationClasses().append(nextClass)
    calculationClasses = result.getCalculationClasses()
    classCountings = [0]*amountOfClasses
    isDownwardsMovement = True
    lastDataPoint = data[0][0]
    for dataLine in data:
        upperBound = lastDataPoint + resetWidth
        lowerBound = lastDataPoint - resetWidth
        if (dataLine[0] > upperBound):
            #the new point is higher than the last one. So the last one was no maxima.
            if(isDownwardsMovement):
                #We iterate through the classes starting with the lowest one
                for i in range(amountOfClasses):
                    #If the class upper limit is lower
                    print("class %s [%s - %s]" % (i,calculationClasses[i].getMin(),calculationClasses[i].getMax()))
                    if ((lastDataPoint <= calculationClasses[i].getMax()) & (lastDataPoint >= calculationClasses[i].getMin())):
                        print("point %s <= %s" % (lastDataPoint,calculationClasses[i].getMax()))
                        classCountings[i] += 1
                        break
            isDownwardsMovement = False
        elif (dataLine[0] < lowerBound):
            #the new dataPoint is lower than the last one.
            #If the last movement was upwards, the last point was a maxima because it is going downwards now
            isDownwardsMovement = True
            #Found starting point (first maximum)
        #Always set the last data point. It is needed for the next bounds calculation
        lastDataPoint = dataLine[0]
    #handle the results
    result.setClassCrossings(classCountings)
    return result

    