#!/usr/bin/env python
# -*- coding: utf-8 -*- 
#@license
#Copyright (c) 2018 Bernhard Kinast. All rights reserved.
#Except where otherwise noted, this work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
#To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
#or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
from helpers.counting_procedure_result import CountingProcedureResult, CountingProcedureClass

def calculate(data=["value"],min=0.0,max=250.0,amount=8):
    """Die Klassengrenzenüberschreitungszählung ist ein einparametriges Zählverfahren"""
    print("Starting counting procedure klassengrenzen_rueckstellbreite")
    print("value of min: %s of type %s" % (min, type(min)))
    print("value of max: %s of type %s" % (max, type(max)))
    print("value of amount: %s of type %s" % (amount, type(amount)))
    result = CountingProcedureResult()
    step = float(max-min)/amount
    for i in range(0,amount):
        nextClass = CountingProcedureClass()
        nextClass.setMin(i*step+min)
        nextClass.setMax((i+1)*step+min)
        result.getCalculationClasses().append(nextClass)
    calculationClasses = result.getCalculationClasses()
    #usedIndizes = result.getUsedIndizes()
    #addedIndex = False
    pointsOfInterest = result.getPointsOfInterest()
    classCrossings = [0]*amount
    foundFirstmaximum = False
    lastDataPoint = data[0][0]
    lastMaximum = data[0][0]
    index = 0
    for dataLine in data:
        
        #addedIndex=False
        upperBound = lastMaximum
        lowerBound = lastMaximum
        if (dataLine[0] > upperBound and dataLine[0] > lastDataPoint):
            if foundFirstmaximum:
                for i in range(amount,0,-1):
                    if calculationClasses[i-1].getMin() <= lastDataPoint:
                        break
                    classUpperBound = calculationClasses[i-1].getMin()
                    if dataLine[0] > classUpperBound and dataLine[0]-lastDataPoint!=0:
                        pointsOfInterest.append([index-1+(classUpperBound-lastDataPoint)/(dataLine[0]-lastDataPoint),classUpperBound])
                        classCrossings[i-1] += 1
                        lastMaximum = dataLine[0]
                        #if not addedIndex:
                        #    addedIndex = True
                        #    usedIndizes.append(index)
            else:
                #no starting point found yet!
                pass
        elif (dataLine[0] < lowerBound):
            foundFirstmaximum = True
            lastMaximum =float('-inf')
            
        lastDataPoint = dataLine[0]
        index += 1
            #Found starting point (first maximum)
    result.setClassCrossings(classCrossings)
    return result

    